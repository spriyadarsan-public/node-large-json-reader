/*jshint esversion: 6 */

const JSONStream = require('JSONStream');
const es = require('event-stream');
const fs = require('fs');

const filePath = 'temp/products.json';
const outputFilePath = 'temp/output.json';

let counter = 0;

fs.unlink(outputFilePath, err => {
  if (err) {
    console.error(err);
    return;
  }
});

fs.appendFile(outputFilePath, '{\r\n', err => {});

fileStream = fs.createReadStream(filePath, { encoding: 'utf8' });
fileStream.pipe(JSONStream.parse('*')).pipe(
  es.through(function(data) {
    this.pause();
    processOneCustomer(data, this);
    return data;
  })
);

fileStream.on('end', () => {
  console.log('There will be no more data.');
  fs.appendFile(outputFilePath, '}', err => {});
});

let processOneCustomer = (product, es) => {
  try {
    const length = 20;
    // var result = '';
    // var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    // var charactersLength = characters.length;
    // for (var i = 0; i < length; i++) {
    //   result += characters.charAt(Math.floor(Math.random() * charactersLength));
    // }
    // let id = '-L5b98' + result;
    delete product._id;
    delete product._class;
    // let newProduct = {};
    // newProduct[id] = product;
    let jsonString = JSON.stringify(product);
    jsonString = jsonString.substring(1, jsonString.length - 1) + ',\r\n';
    fs.appendFileSync(outputFilePath, jsonString);
    console.log(++counter);
    es.resume();
  } catch (err) {
    console.log('Error writing to file', err);
  }
};
